## PyGimbal
PyGimbal is a REST client for Qualcomm's Gimbal geofencing API.

Dependencies:
You need to have the 'requests' and 'simplejson' Python modules installed.

Usage:

```python
import gimbal

_gimbal = gimbal.init("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", False)

#The first parameter for init is your Gimbal API token, the second specifies whether you are using the
#live API or development, True for live, False for development. (defaults to development)

comm = _gimbal.communications()
comm_data = {
    "start_date" : "2013-04-18",
    "end_date" : "2013-04-19",
    "name" : "My New Communicate"
}
comm.createCommunication(comm_data)


fences = _gimbal.geofences()
fence = {
    "name": "Gimbal HQ",
    "addressLineOne": "5775 Morehouse Drive, San Diego CA 92121",
    "geoFenceCircle": {
        "radius": 100,
        "location": {
            "latitude": 32.89494374592149,
            "longitude": -117.19603832579497
        }
    },
    "placeAttributes": {
        "key1": "value1",
        "key2": "value2"
    }
}
fences.createGeofence(fence)

```

###Current implemented calls:

#### gimbal.communications
* getAll()
* getCommunication(comm_id)
* updateCommunication(comm_id, comm)
* deleteCommunication(comm_id)
* createCommunication(comm)
* getTargets(comm_id)
* createTarget(comm_id, targets)
* deleteTargets(comm_id)
* getTriggers(comm_id)
* createTrigger(comm_id, trigger)
* deleteTargets(comm_id)

#### gimbal.geofences
* getAll()
* updateGeofence(gf_id, fence)
* deleteGeofence(gf_id)
* getGeofence(gf_id)
* createGeofence(fence)


