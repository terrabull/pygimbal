import gimbal
import time

_gimbal = gimbal.init("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", False)
c = _gimbal.communications()

def getAll():
    return c.getAll()

def getComm(comm_id):
    return c.getCommunication(comm_id)

def createComm():
    comm = {
        "start_date" : "2013-04-18",
        "end_date" : "2013-04-19",
        "name" : "My New Communicate"
    }
    return c.createCommunication(comm)

def deleteComm(comm_id):
    return c.deleteCommunication(comm_id)

def updateComm(comm_id, comm):
    return c.updateCommunication(comm_id, comm)

def getTargets(comm_id):
    return c.getTargets(comm_id)


if __name__ == "__main__":
    results = getAll()
    print "\nShowing all communications: "

    for comm in results:
        print comm.get("name")
        #print getComm(comm.get("id"))
        print "\nUpdating communication"
        comm_id = comm.get("id")
        new_name = comm.get("name")
        new_name+= "."
        updateComm(comm_id, {"name": new_name})
        print "\nShowing targets"
        getTargets(comm_id)

    print "\nCreating new communication"
    result = createComm()
    delete_id = result.get("id")
    print "\nDeleting communication"
    deleteComm(delete_id)

