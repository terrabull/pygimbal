from _rest import rest

class communications:
    def __init__(self, token, is_live):
        self.rest = rest(token, is_live)

    def getAll(self):
        return self.rest.get("communications")

    def getCommunication(self, comm_id):
        return self.rest.get("communications/%s" % comm_id)

    def updateCommunication(self, comm_id, comm):
        return self.rest.put("communications/%s" % comm_id, comm)

    def deleteCommunication(self, comm_id):
        return self.rest.delete("communications/%s" % comm_id)

    def createCommunication(self, comm):
        return self.rest.post("communications", comm)

    def getTargets(self, comm_id):
        return self.rest.get("communications/%s/geofence_targets" % comm_id)

    def createTarget(self, comm_id, targets):
        # {"target_ids": [33333333333667]}
        return self.rest.post("communications/%s/geofence_targets" % comm_id, targets)

    def deleteTargets(self, comm_id):
        return self.rest.delete("communications/%s/geofence_targets" % comm_id)

    def getTriggers(self, comm_id):
        return self.rest.get("communications/%s/geofence_trigger" % comm_id)

    def createTrigger(self, comm_id, trigger):
        """
        {
            "event_type": "at",
            "locations": [16144]
        }
        """
        return self.rest.post("communications/%s/geofence_trigger" % comm_id, trigger)


    def deleteTargets(self, comm_id):
        return self.rest.delete("communications/%s/geofence_trigger" % comm_id)

