import simplejson
import requests
from _config import config

class rest:
    def __init__(self, token, is_live):
        self.config = config(token, is_live)

    def get(self, path):
        fullpath = "%s%s" % (self.config.getAPIUrl(), path)
        response = requests.get(fullpath, headers=self.config.getAPIHeaders())
        return self.parseResponse(response)

    def delete(self, path):
        fullpath = "%s%s" % (self.config.getAPIUrl(), path)
        response = requests.delete(fullpath, headers=self.config.getAPIHeaders())
        return self.parseResponse(response)

    def post(self, path, params={}):
        fullpath = "%s%s" % (self.config.getAPIUrl(), path)
        response = requests.post(fullpath, data=simplejson.dumps(params), headers=self.config.getAPIHeaders())
        return self.parseResponse(response)

    def put(self, path, params={}):
        fullpath = "%s%s" % (self.config.getAPIUrl(), path)
        response = requests.put(fullpath, data=simplejson.dumps(params), headers=self.config.getAPIHeaders())
        return self.parseResponse(response)

    def parseResponse(self, response):
        try:
            return response.json()
        except:
            print response.text
            return {}

