from _rest import rest

class geofences:
    def __init__(self, token, is_live):
        self.rest = rest(token, is_live)

    def getAll(self):
        return self.rest.get("geofences")

    def updateGeofence(self, gf_id, fence):
        return self.rest.put("geofences/%s" % gf_id, fence)

    def deleteGeofence(self, gf_id):
        return self.rest.delete("geofences/%s" % gf_id)

    def getGeofence(self, gf_id):
        return self.rest.get("geofences/%s" % gf_id)

    def createGeofence(self, fence):
        return self.rest.post("geofences", fence)





