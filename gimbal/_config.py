
class config:
    token = ""
    is_live = False
    def __init__(self, token = "", is_live = False):
        if token:
            self.setToken(token)
        self.setLive(is_live)

    def setToken(self, token):
        self.token = token

    def setLive(self, is_live):
        self.is_live = is_live

    def getAPIUrl(self):
        if self.is_live:
            return "https://manager.gimbal.com/api/"
        else:
            return "https://sandbox.gimbal.com/api/"

    def getAPIHeaders(self):
        return {'Authorization': 'Token token=%s' % self.token, 'Content-Type': 'application/json'}

