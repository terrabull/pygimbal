import requests
from _geofences import geofences
from _communications import communications

#__all__ = ["geofences", "communications"]


class init:
    def __init__(self, token, is_live):
        self._geofences = geofences(token, is_live)
        self._communications = communications(token, is_live)

    def communications(self):
        return self._communications

    def geofences(self):
        return self._geofences
