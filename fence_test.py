import gimbal
import time

_gimbal = gimbal.init("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", False)
g = _gimbal.geofences()

def createFence():
    fence = {
        "name": "Gimbal HQ",
        "addressLineOne": "5775 Morehouse Drive, San Diego CA 92121",
        "geoFenceCircle": {
            "radius": 100,
            "location": {
                "latitude": 32.89494374592149,
                "longitude": -117.19603832579497
            }
        },
        "placeAttributes": {
            "key1": "value1",
            "key2": "value2"
        }
    }
    return g.createGeofence(fence)

def getAll():
    return g.getAll()


def getFence(gf_id):
    return g.getGeofence(gf_id)

def deleteFence(gf_id):
    return g.deleteGeofence(gf_id)

def updateFence(gf_id, fence):
    return g.updateGeofence(gf_id, fence)

if __name__ == "__main__":
    result = getAll()


    print "\nShowing all geofences: "

    for f in result["geofences"]:
        print f.get("name")
        fence = {
            "placeAttributes": {
                "now": time.time()
            }
        }
        fence_id = f.get("id")
        print "\nUpdating geofence %s" % fence_id
        updateFence(fence_id, fence)

    print "\nCreating new geofence"
    result = createFence()
    delete_id = result.get("id")
    print "\nDeleting new geofence"
    print deleteFence(delete_id)







